interface Cat {
  image: string,
  price: number,
  id: string,
}

interface CartItem extends Cat {
  quantity: number,
}

interface Cart {
  totalAmount: number,
  catsList: CartItem[]
}

let catsList: Cat[] = [];
let cart: Cart = { totalAmount: 0, catsList: [] };

const retrieveData = (): void => {
  const savedCatsList = localStorage.getItem('catsList');
  const savedCart = localStorage.getItem('cart');
  if (savedCatsList) { catsList = JSON.parse(savedCatsList); }
  if (savedCart) { cart = JSON.parse(savedCart); }
};

const initFakeApiData = (): void => {
  retrieveData();
  if (catsList.length === 0) {
    catsList = [];
    localStorage.setItem('catsList', JSON.stringify(catsList));
    for (let i = 0; i < 10; i += 1) {
      catsList.push({
        image: `fake-api/mocks/images/cat${i + 1}.png`,
        price: (i + 1) * 10,
        id: `${i}`,
      } as Cat);
    }
    if (cart.catsList.length === 0) {
      cart = { totalAmount: 0, catsList: [] };
      localStorage.setItem('cart', JSON.stringify(cart));
    }
  }

  localStorage.setItem('catsList', JSON.stringify(catsList));
  localStorage.setItem('cart', JSON.stringify(cart));
};

const saveCart = (): void => {
  localStorage.setItem('cart', JSON.stringify(cart));
};

const getCatsList = (): Cat[] => catsList;

const getCart = (): Cart => cart;

interface PostCatParameters {
  id: string
}

const postCatToCart = ({ id }: PostCatParameters): Cart => {
  const alreadyAddedCatIndex = cart.catsList.findIndex((cat) => cat.id === id);
  if (alreadyAddedCatIndex !== -1) {
    cart.catsList[alreadyAddedCatIndex].quantity += 1;
    cart.totalAmount += cart.catsList[alreadyAddedCatIndex].price;
  } else {
    const catToAdd = catsList.find((cat) => cat.id === id);

    if (catToAdd) {
      cart.totalAmount += catToAdd.price;
      cart.catsList.push({ ...catToAdd, quantity: 1 });
    }
  }

  saveCart();
  return cart;
};

interface PostCatParameters {
  id: string
}

const deleteCatFromCart = ({ id }: PostCatParameters): Cart => {
  const removeCatAtIndex = cart.catsList.findIndex((cat) => cat.id === id);

  if (removeCatAtIndex !== -1) {
    cart.totalAmount -= cart.catsList[removeCatAtIndex].price;
    cart.catsList.splice(removeCatAtIndex, 1);
  }

  saveCart();
  return cart;
};

export default {
  initFakeApiData,
  getCatsList,
  postCatToCart,
  deleteCatFromCart,
  getCart,
};
