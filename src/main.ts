import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Api from './fake-api';

Vue.config.productionTip = false;

// Initialize data in fake api
Api.initFakeApiData();

new Vue({
  router,
  store,
  render: (h): any => h(App),
}).$mount('#app');
