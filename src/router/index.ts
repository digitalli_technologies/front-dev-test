import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import ShopPage from '../pages/ShopPage.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: '/shop',
  },
  {
    path: '/shop',
    name: 'ShopPage',
    component: ShopPage,
  },
  {
    path: '*',
    redirect: '/',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
