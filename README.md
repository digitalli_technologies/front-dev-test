# 🎯 RCI Front-end Vue.js test
Hi, please find all informations about the tests below, please read **CAREFULLY** all the instructions to be sure to submit a test which fullfills the expectations of the reviewer.
This test is supposed to be submitted in **Vue.js typescript and be done within a 4 hours timeframe**

## 🛠 Project requirements

### 1. Install the following tools

- `Git`
- `Node >= v14.15.4`
- `Npm >= 6.14.10`

### 2. Clone the repository

```
git clone https://SachaHZ@bitbucket.org/rci_technologies/front-dev-test.git
```

### 3. Install depedencies

```
yarn
```

### 4. Run the server

```
yarn serve
```

### 5. Verify the ouput

```
No type errors found
Version: typescript 4.1.5
Time: 6728ms

  App running at:
  - Local:   http://localhost:3000/
  - Network: http://localhost:3000/

  Note that the development build is not optimized.
  To create a production build, run yarn build.
```

### 6. Check App is running well

If you go to [http://localhost:3000/](http://localhost:3000/ ) you should see a white page containing the following bold text `Ready to go !`.

If you make a modification to `App.vue` file then it should reloads automatically on your browser thanks to **hot reload**.

### 7. Configure VSCode (Optional)

The internal front-end team mainly works on [Visual Studio Code](https://code.visualstudio.com/) on MacOS, if you feel confident in setting up the project in your environment, please ignore this section, otherwise please proceed with the following configurations.

### 8. Install extensions

First of all download the following extensions:
- **eslint** `dbaeumer.vscode-eslint`
- **stylelint** from `stylelint.vscode-stylelint`
- **Vetur** from `octref.vetur`
- **vue** from `jcbuisson.vue`

### 9. Modify settings.json

Once all the extensions are downloaded, you will need to **append** your visual studio code `settings.json` file with the following :

```
{
    // EDITOR
    // ----------------------------------------
    "editor.defaultFormatter": "dbaeumer.vscode-eslint",
    "[javascript]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
    "[typescript]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
    "[vue]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
    "[scss]": { "editor.defaultFormatter": "stylelint.vscode-stylelint" },
    "[css]": { "editor.defaultFormatter": "stylelint.vscode-stylelint" },
    "editor.codeActionsOnSave": {
    // https://github.com/microsoft/vscode-eslint/blob/master/README.md#release-notes
    "source.fixAll.eslint": true,
    "source.fixAll.stylelint": true
    },

    // ESLINT
    // ----------------------------------------
    "eslint.validate": ["typescript"],
    "eslint.alwaysShowStatus": true,
    "eslint.options": {
    "extensions": [".html", ".js", ".ts", ".vue"]
    },

    // VETUR
    // Disable rules if user has extension installed and enabled.
    // ----------------------------------------
    "vetur.validation.template": false,
    "vetur.validation.style": false,
    "vetur.format.defaultFormatter.html": "none",
    "vetur.format.defaultFormatter.css": "none",
    "vetur.format.defaultFormatter.scss": "none",
    "vetur.format.defaultFormatter.js": "none",
    "vetur.format.defaultFormatter.ts": "none",

    // STYLELINT
    // ----------------------------------------
    "stylelint.enable": true,
    "css.validate": true,
    "scss.validate": true,

    // HTML
    // ----------------------------------------
    "html.format.enable": false,
    "emmet.triggerExpansionOnTab": true,
    "emmet.includeLanguages": {
    "vue-html": "html"
    },

    // FILES
    // ----------------------------------------
    "files.exclude": {
    "**/*.log": true,
    "**/*.log*": true,
    "**/dist": true,
    },
    "files.associations": {
    ".babelrc": "jsonc",
    ".eslintrc": "jsonc",
    ".markdownlintrc": "jsonc",
    "*.config.js": "javascript",
    "*.spec.js": "javascript",
    "*.vue": "vue"
    },
    // The default end of line character. Use \n for LF and \r\n for CRLF.
    "files.eol": "\n",
    "files.insertFinalNewline": true,
    "files.trimFinalNewlines": true,
    "files.trimTrailingWhitespace": true,

    "git.autofetch": true,
    "gitlens.views.branches.branches.layout": "list",
    "vetur.completion.tagCasing": "initial",
}
```


**Save the file then reboot Visual Studio Code**

> The following allows you to have a more comfortable setup by auto fixing all issues and linting them while coding by using our own eslint/stylint chart



After these steps are completed you are finally free to go and start your technical test.



## 🏆 Test goals

You will be given a set of features to implement, the goal is not to submit the best design and/or the highest visual fidelity but to complete all asked features implementations by providing them with a good architecture, readable code, reusability, maintainability, etc ...

**You will be graded on the following points.**
- **Components splitting**
- **Data management with vuex**
- **State mutations, reactivity**
- **Data sharing between components**
- **Code readability, maintainibility**
- **Style management, scss variables**
- **Template management, html semantic**
- **Relevance of typing**

## ⛩ Project structure & tools

You will see that the current setup provides everything you need to get to the expected result. Feel free to add packages if you feel like there would be relevant, but please note that for every added package a justification will be asked and an answer will be expected.

Normally you should only modify files inside the `src/` directory, the rest should stay untouched.
Here is the current `src` file structure.
```
src/
├── components/
    // Contains all components of your app
├── fake-api/
    // You shouldn\'t need to do any modifications inside that folder
        this is just the service which \'emulates\' the api,
        so please only import to use the methods
        which will enable you to get, post, delete datas
├── pages/
    // Contains all pages of your app
    ├── ShopPage.vue
├── router/
    // App router
    ├── index.ts
├── store/
    // App store
    ├── index.ts
├── styles/
    // App styles
    ├── index.ts
├── App.vue
├── main.ts // Shouldn\'t need any modifications
├── shims-tsx.d.ts  // Shouldn\'t need any modifications
├── shims-vue.d.ts  // Shouldn\'t need any modifications
```

## 👌 Before beginning

We would recommend you to first :

*  Read once more the above specifications, to be sure you fully understand what we expect from you, and what you will be graded on
* Check the already existing files, package.json to familiarize yourself with the project
* As we said before every packages present in the project helps you reaching the expected goal, so please have a look at already integrated packages before installing others
* Set a 4 hours timer
* Take notes, it would be much appreciated to provide a short feedback on the following points :
    * Did you finish implementing all the features ?
    * What was the hardest ?
    * What was the simplest ?
    * What was your favorite coding part ?
    * What did you find interesting ?
    * How would you rate this test on a score of 10 ?
    * How would you rate what you implemented on a score of 10 ?
    * Did you add any extra packages ? If so, can you explain why for each of these packages ?

## 🚀 Features to implement

During this test you add a feature to the current empty website **CryptoKitties**. They sell good. You will provide a list with those cute animals and when clicking on them it will add it to a **Shopping Cart**, a single model of kitty can vary in quantity if selected multiple times. You can also remove kitties from your cart. All those operations. Fair simple right ?

To implement those features you will be using the `fake-api` service. You will notice that this service doesn't return any `Promises`, actually it's just using the local storage, but please call its methods as it was a "true" api service.

### 🗓 Tasks

To help you visualize the following tasks some pictures will be attached, having the same visual render is not what matters to us as mentioned above we will be evaluating you only from a technical perspective and not a product one.


### **TASK 1** Implement the list of kitties

**GIVEN** I am on shop page


**THEN** I should see the list of kitties




![List of kitties](./utils_readme/task-1.png "List of kitties")



### **TASK 2** Implement the cart

**GIVEN** I am on shop page


**THEN** I should see my cart and its items




![List of kitties](./utils_readme/task-2.png "Cart")




### **TASK 3** Add cat to cart

**GIVEN** I am on shop page

**WHEN** I click on a cat

**THEN** It should add a cat to my cart

**AND** update the total amount of my cart


![List of kitties](./utils_readme/task-3.png "Cart 1 item")


### **TASK 4** Change cat quantity in cart

**GIVEN** I am on shop page

**WHEN** I click on a cat

**AND** the cat was already added

**THEN** It should change quantity of this cat in my cart

**AND** update the total amount of my cart



![List of kitties](./utils_readme/task-4.png "Cart 1 item")



### **TASK 5** Remove cat from cart

**GIVEN** I am on shop page

**AND** My cart contains at least one cat

**WHEN** I click on remove cat button in my cart

**THEN** It should remove cat from the cart

**AND** update the total amount of my cart



![List of kitties](./utils_readme/task-5.png "Cart 1 item")


# 🍀 Good Luck to you !
